import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'POC Numen OpenLayers';

  images = [
    {dir: '04bi719'},               // ok, facture
    {dir: '03bi700'},               // ok+, facture
    {dir: '10ve921'},               // ok+, courrier changement de coordonnées bancaires
    {dir: '08in013'},               // ok+, bilan impôts (tableau)
    {dir: '09in795'},               // ok+, bilan consolidation (tableau)
    {dir: '06di680'},               // ok, journal officiel 1989
    {dir: '05di100'},               // ok-, journal officiel 1941
    {dir: '01af377'},               // ok-, fiche d'état civil
    {dir: '02af487'},               // ocr mauvais, adresse sur enveloppe
    {dir: '07ge668', type: 'jpg'},  // ocr très mauvais, lands commission
    {dir: '11ve347'},               // ocr inutilisable, lettre manuscrite
    {dir: '12fo520'},               // ok, facture
  ]
}
