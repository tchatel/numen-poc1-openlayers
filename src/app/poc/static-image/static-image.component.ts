import {Component, OnInit} from '@angular/core';
import {CaptureService} from "../capture.service";

@Component({
  selector: 'app-static-image',
  templateUrl: './static-image.component.html',
  styleUrls: ['./static-image.component.css']
})
export class StaticImageComponent implements OnInit {

  map: ol.Map;
  layers: ol.layer.Image[];

  constructor(private capture: CaptureService) {
  }

  layersInfo() {
    return this.map.getLayers().getArray().map(layer => ` [${layer.getMinResolution()}, ${layer.getMaxResolution()}[ `);
  }

  ngOnInit() {

    const url = this.capture.urlImage('04bi719');

    // Map views always need a projection.  Here we just want to map image
    // coordinates directly to map coordinates, so we create a projection that uses
    // the image extent in pixels.

    // Image avec des marges : 1000 px de chaque côté
    const mapExtent: ol.Extent = [0, 0, 2000 + 2476, 2000 + 3510];
    let mapProjection = new ol.proj.Projection({
      code: 'xkcd-image',
      units: 'pixels',
      extent: mapExtent
    });

    const sourceExtent: ol.Extent = [1000 + 0, 1000 + 0, 1000 + 2476, 1000 + 3510];

    this.map = new ol.Map({
      // layers: this.layers,
      target: 'map',
      view: new ol.View({
        projection: mapProjection,
        center: ol.extent.getCenter(mapExtent),
        resolution: 8,
        minResolution: 1,
        maxResolution: 16,
        // resolutions: [16, 8, 4, 2, 1]
      }),
      layers: [
        new ol.layer.Image({
          source: new ol.source.ImageStatic({
            url: 'assets/data/04bi719/scan.png',
            projection: mapProjection, imageExtent: sourceExtent,
          }),
        })
      ]
    });
  }

}
