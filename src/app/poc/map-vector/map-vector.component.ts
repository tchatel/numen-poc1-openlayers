import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-vector',
  templateUrl: './map-vector.component.html',
  styleUrls: ['./map-vector.component.css']
})
export class MapVectorComponent implements OnInit {

  map: ol.Map;

  constructor() { }

  ngOnInit() {

    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        }),
        new ol.layer.Vector({
          // title: 'Buildings',
          source: new ol.source.Vector({
            url: 'assets/data/buildings.kml',
            format: new ol.format.KML({
              extractStyles: false
            })
          }),
          style: new ol.style.Style({
            stroke: new ol.style.Stroke({color: 'red', width: 2})
          })
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([-122.79264450073244, 42.30975194250527]),
        zoom: 16
      })
    });

  }

}
