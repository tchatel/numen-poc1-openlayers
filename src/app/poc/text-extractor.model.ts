
interface TextExtent {
  text: string;
  extent: ol.Extent; // [minx, miny, maxx, maxy]
  line?: number;
}

export class TextExtractor {

  static extractText(selectedFeatures: ol.Collection<ol.Feature>,
                     textGetter: (feature: ol.Feature) => string,
                     textIndexGetter: (feature: ol.Feature) => number) {
    let featuresArray = selectedFeatures && selectedFeatures.getArray();
    if (!featuresArray) return;
    return featuresArray
      .map(feature => (
        {text: textGetter(feature), index: textIndexGetter(feature)}
      ))
      .sort((o1, o2) => o1.index - o2.index)
      .map(o => o.text)
      .join('');
  }

  // OLD
  static extractTextWithoutIndex(selectedFeatures: ol.Collection<ol.Feature>,
                     textGetter: (feature: ol.Feature) => string,
                     indexGetter: (feature: ol.Feature) => number) {
    let featuresArray = selectedFeatures && selectedFeatures.getArray();
    if (!featuresArray) return;
    let textExtents: TextExtent[] = featuresArray.map(feature => (
      {text: textGetter(feature), extent: feature.getGeometry().getExtent()}
    ));
    let sortedTextExtents = TextExtractor.sortExtentsByLines(textExtents);
    return TextExtractor.joinText(sortedTextExtents);
  }

  private static sortExtentsByLines(textExtents: TextExtent[]) {
    TextExtractor.computeLines(textExtents);
    return textExtents
      .sort((t1, t2) => {
        if (t1.line != t2.line) {
          return t1.line - t2.line;
        } else {
          return t1.extent[0] - t2.extent[0];
        }
      });
  }
  private static joinText(textExtents: TextExtent[]): string {
    let lineIndex = 0;
    let resultText = textExtents
      .reduce((text: string, t: TextExtent) => {
        const trimmedText = t.text.trim();
        const newLine = t.line > lineIndex ? '\n' : '';
        const space = (text && (!trimmedText || t.text.startsWith(' ')) && !text.endsWith(' ')) ? ' ' : '';
        lineIndex = t.line;
        return text + (newLine || space) + trimmedText;
      }, '');
    return resultText;
  }

  // Returns an array of vertical extents which correspond to text lines
  private static computeLines(textExtents: TextExtent[]) {
    let sortedExtents = textExtents
      .map(t => TextExtractor.verticalExtent(t.extent))
      .sort((ext1, ext2) => -(ext1[1] - ext2[1])); // y coord start from bottom
    let lines = TextExtractor.mergeExtents(sortedExtents);
    textExtents.forEach(t => {
      let yExtend = TextExtractor.verticalExtent(t.extent);
      let line = lines.filter(r => ol.extent.intersects(r, yExtend))[0];
      if (!line) {
        throw 'bug : no line found';
      }
      t.line = lines.indexOf(line);
    });
  }

  // Returns an extent with xmin=0 and xmax=1, and height reduce by 50%
  private static verticalExtent(extent: ol.Extent): ol.Extent {
    const deltaY = extent[3] - extent[1];
    const margin = Math.round(deltaY / 4);
    return [0, extent[1] + margin, 1, extent[3] - margin];
  }

  // Merge overlapping extents, from a SORTED array of extents
  private static mergeExtents(sortedExtents: ol.Extent[]): ol.Extent[] {
    let first = sortedExtents[0];
    let second = sortedExtents[1];
    let remainingArray = sortedExtents.slice(2);
    if (sortedExtents.length < 2) {
      return sortedExtents;
    } else if (ol.extent.intersects(first, second)) {
      return TextExtractor.mergeExtents([ol.extent.extend(first, second), ...remainingArray]);
    } else {
      return [first, ...TextExtractor.mergeExtents([second, ...remainingArray])];
    }
  }

}