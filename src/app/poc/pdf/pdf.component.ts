import {Component, OnInit, AfterViewInit} from '@angular/core';

declare let PDFJS: any;

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit, AfterViewInit {

  // PDF
  pdfDoc: any = null;
  pageNum = 1;
  pageRendering = false;
  pageNumPending: number = null;
  scale = 2; //0.8;   // Détermine les dimensions du canvas du PDF (2) => 1224x1584
  pdfCanvas: HTMLCanvasElement;
  pdfCanvasContext: CanvasRenderingContext2D;

  get pageCount() {
    return this.pdfDoc && this.pdfDoc.numPages;
  }

  // Map
  map: ol.Map;
  extent: ol.Extent = [0, 0, 1224, 1584];
  // extent: ol.Extent = [0, 0, 612, 792];
  projection: ol.proj.Projection;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.pdfInit();
    this.mapInit();
  }

  mapInit() {
    this.projection = new ol.proj.Projection({
      code: 'xkcd-image',
      units: 'pixels',
      extent: this.extent
    });
    this.map = new ol.Map({
      target: 'map',
      view: new ol.View({
        projection: this.projection,
        center: ol.extent.getCenter(this.extent),
        resolution: 2,
        maxResolution: 16
      })
    });
  }

  mapSetLayer(canvas: HTMLCanvasElement) {
    canvas.toBlob((blob) => {
      const url = window.URL.createObjectURL(blob);
      const source = new ol.source.ImageStatic({
        url: url,
        projection: this.projection,
        imageExtent: this.extent
      });
      let layer = this.map.getLayers().item(0) as ol.layer.Image;
      if (layer) {
        layer.setSource(source);  // Only on a ol.layer.Image
      } else {
        this.map.addLayer(new ol.layer.Image({source}));
      }
    });
  }

  pdfInit() {
    // If absolute URL from the remote server is provided, configure the CORS
    // header on that server.
    let url = 'assets/tracemonkey.pdf';
    // let url = '//cdn.mozilla.net/pdfjs/tracemonkey.pdf';

    // The workerSrc property shall be specified.
    PDFJS.workerSrc = 'assets/pdf.worker.js';
    // PDFJS.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

    this.pdfCanvas = document.getElementById('pdf-canvas') as HTMLCanvasElement;
    this.pdfCanvas.width = ol.extent.getWidth(this.extent);
    this.pdfCanvas.height = ol.extent.getHeight(this.extent);
    this.pdfCanvasContext = this.pdfCanvas.getContext('2d');

    /**
     * Asynchronously downloads PDF.
     */
    PDFJS.getDocument(url).then((pdfDoc) => {
      this.pdfDoc = pdfDoc;
      // Initial/first page rendering
      this.renderPage(this.pageNum);
    });
  }

  /**
   * Get page info from document, resize pdfCanvas accordingly, and render page.
   * @param num Page number.
   */
  renderPage(num) {
    this.pageRendering = true;
    // Using promise to fetch the page
    this.pdfDoc.getPage(num).then((page) => {
      let viewport = page.getViewport(this.scale);
      this.pdfCanvas.height = viewport.height;
      this.pdfCanvas.width = viewport.width;

      // Render PDF page into pdfCanvas context
      let renderContext = {
        canvasContext: this.pdfCanvasContext,
        viewport: viewport
      };
      let renderTask = page.render(renderContext);

      // Wait for rendering to finish
      renderTask.promise.then(() => {
        this.pageRendering = false;
        if (this.pageNumPending !== null) {
          // New page rendering is pending
          this.renderPage(this.pageNumPending);
          this.pageNumPending = null;
        }
        this.mapSetLayer(this.pdfCanvas);
      });
    });
  }

  /**
   * If another page rendering in progress, waits until the rendering is
   * finised. Otherwise, executes rendering immediately.
   */
  queueRenderPage(num: number) {
    if (this.pageRendering) {
      this.pageNumPending = num;
    } else {
      this.renderPage(num);
    }
  }

  previousPage() {
    if (this.pageNum <= 1) {
      return;
    }
    this.pageNum--;
    this.queueRenderPage(this.pageNum);
  }

  nextPage() {
    if (this.pageNum >= this.pdfDoc.numPages) {
      return;
    }
    this.pageNum++;
    this.queueRenderPage(this.pageNum);
  }

}
