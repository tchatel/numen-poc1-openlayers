import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MapComponent} from './map/map.component';
import {StaticImageComponent} from './static-image/static-image.component';
import {MapVectorComponent} from './map-vector/map-vector.component';
import {ImageVectorComponent} from './image-vector/image-vector.component';
import {CaptureService} from "./capture.service";
import {FormsModule} from "@angular/forms";
import {TileImageComponent} from "./tile-image/tile-image.component";
import {OptionsService} from "./options.service";
import { MemoryTilesComponent } from './memory-tiles/memory-tiles.component';
import { CanvasTestComponent } from './canvas-test/canvas-test.component';
import { PostitComponent } from './postit/postit.component';
import { PdfComponent } from './pdf/pdf.component';
import { IndexComponent } from './index/index.component';
import {RouterModule} from '@angular/router';
import {ScaledImageComponent} from './scaled-image/scaled-image.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
    MapComponent,
    StaticImageComponent,
    ScaledImageComponent,
    MapVectorComponent,
    ImageVectorComponent,
    TileImageComponent,
    MemoryTilesComponent,
    CanvasTestComponent,
    PostitComponent,
    PdfComponent,
    IndexComponent,
  ],
  exports: [
    MapComponent
  ],
  providers: [
    CaptureService,
    OptionsService,
  ],
})
export class PocModule {
}
