import { Injectable } from '@angular/core';

@Injectable()
export class OptionsService {

  showImageLayer = true;
  showVectorLayer = true;

}
