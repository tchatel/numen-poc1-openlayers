import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageVectorComponent } from './image-vector.component';

describe('ImageVectorComponent', () => {
  let component: ImageVectorComponent;
  let fixture: ComponentFixture<ImageVectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageVectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageVectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
