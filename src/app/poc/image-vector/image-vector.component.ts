import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {CaptureService, Capture} from "../capture.service";
import {ActivatedRoute} from "@angular/router";
import "rxjs/add/operator/map";
import "rxjs/add/operator/switchMap";
import {TextExtractor} from "../text-extractor.model";
import {OptionsService} from "../options.service";

interface MapData {
  directory: string;
  imageUrl: string;
  capture: Capture;
}

@Component({
  selector: 'app-image-vector',
  templateUrl: './image-vector.component.html',
  styleUrls: ['./image-vector.component.css']
})
export class ImageVectorComponent implements OnInit {

  @ViewChild('form') form: ElementRef;

  map: ol.Map;
  imageLayer: ol.layer.Image;
  vectorLayer: ol.layer.Vector;
  mapData: MapData;
  select: ol.interaction.Select;
  selectedFeatures: ol.Collection<ol.Feature>;
  selectedText: string;
  newLine = true;
  mapZoom: ol.Map;
  selectZoom: ol.interaction.Select;

  fields: {name: string; value: string;}[];
  fieldIndex = 0;

  showBoxStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({color: 'red', width: 1})
  });
  hideBoxStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({color: [255, 0, 0, 0.1], width: 0})
  });
  selectBoxStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({color: [255, 50, 50, 0.7], width: 1}),
    fill: new ol.style.Fill({color: [255, 10, 10, 0.3]})
  });


  constructor(
    private capture: CaptureService,
    private route: ActivatedRoute,
    private options: OptionsService,
  ) { }

  getSelectedText(newLine: boolean = true) {
    return newLine
      ? this.selectedText && this.selectedText.replace(/\n/g, '<br>')
      : this.selectedText;
  }


  toggleLayers() {
    if (this.map) {
      this.imageLayer.setVisible(this.options.showImageLayer);
      this.vectorLayer.setStyle(this.options.showVectorLayer ? this.showBoxStyle : this.hideBoxStyle);
    }
  }

  ngOnInit() {
    this.route.params
      .switchMap(params => {
        const directory = params['directory'];
        const type = params['type'];
        const imageUrl = this.capture.urlImage(directory, type);
        return this.capture.loadFeatures(directory)
          .map((capture: Capture): MapData => ({directory, imageUrl, capture}));
      })
      .subscribe((mapData: MapData) => {
        this.mapData = mapData;
        this.init(mapData);
      });
  }

  init(mapData: MapData) {

    this.map && this.map.setTarget(null);
    this.mapZoom && this.mapZoom.setTarget(null);

    // Map views always need a projection.  Here we just want to map image coordinates directly
    // to map coordinates, so we create a projection that uses the image extent in pixels.
    const extent: ol.Extent = [0, 0, mapData.capture.width, mapData.capture.height];
    let projection = new ol.proj.Projection({
      code: 'xkcd-image',
      units: 'pixels',
      extent: extent
    });

    this.imageLayer = new ol.layer.Image({
      source: new ol.source.ImageStatic({
        attributions: '',
        url: mapData.imageUrl,
        projection: projection,
        imageExtent: extent
      })
    });
    this.vectorLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: mapData.capture.features,
      }),
      // style: this.options.showVectorLayer ? this.showBoxStyle : this.hideBoxStyle
    });
    this.map = new ol.Map({
      layers: [
        this.imageLayer,
        this.vectorLayer,
      ],
      target: 'map',
      view: new ol.View({
        // projection: projection,
        // center: ol.extent.getCenter(extent),
        // zoom: 2,
        // maxZoom: 8,
        projection: projection,
        extent: extent,
        center: ol.extent.getCenter(extent),
        resolution: 4,
        minResolution: 1,
        maxResolution: 16,
        resolutions: [16, 8, 4, 2, 1]
      }),
      controls: [], // disable default controls (zoom & attribution)
    });
    this.toggleLayers();

    this.mapZoom = new ol.Map({
      layers: [
        this.imageLayer,
        this.vectorLayer,
      ],
      target: 'map-zoom',
      view: new ol.View({
        projection: projection,
        extent: extent,
        center: ol.extent.getCenter(extent),
        resolution: 1,
        minResolution: 1,
        maxResolution: 16,
        // resolutions: [16, 8, 4, 2, 1]
      }),
      controls: [], // disable default controls (zoom & attribution)
    });
    this.selectZoom = new ol.interaction.Select({
      layers: [],
      style: this.selectBoxStyle
    });
    this.mapZoom.addInteraction(this.selectZoom);
    this.selectZoom.on('select', () => {
      this.afterSelect();
    });

    // Suppression de l'antialiasing : aucun effet visible ?
    // this.map.on('precompose', (evt) => {
    //   evt.context.imageSmoothingEnabled = false;
    //   evt.context.webkitImageSmoothingEnabled = false;
    //   evt.context.mozImageSmoothingEnabled = false;
    //   evt.context.msImageSmoothingEnabled = false;
    // });



    // SELECTION (ZONE or CLICK) ----------------------------------------
    // a normal select interaction to handle click
    this.select = new ol.interaction.Select({
      layers: [this.vectorLayer],
      style: this.selectBoxStyle
    });
    this.map.addInteraction(this.select);
    this.selectedFeatures = this.select.getFeatures();
    // Select one feature by single click
    this.select.on('select', () => {
      this.afterSelect();
    });

    // a DragBox interaction used to select features by drawing boxes
    let dragBox = new ol.interaction.DragBox({
      condition: ol.events.condition.platformModifierKeyOnly
    });
    this.map.addInteraction(dragBox);
    dragBox.on('boxend', () => {
      // features that intersect the box are added to the collection of selected features
      let selectedExtent = dragBox.getGeometry().getExtent();
      this.vectorLayer.getSource().forEachFeatureIntersectingExtent(selectedExtent, (feature) => {
        this.selectedFeatures.push(feature);
      });
      this.afterSelect();
    });
    // clear selection when drawing a new box and when clicking on the map
    dragBox.on('boxstart', () => {
      this.selectedFeatures.clear();
    });
    this.map.on('click', () => {
      this.selectedFeatures.clear();
    });


    this.selectedText = '';
    // Sample form
    this.initForm();
  }

  // ---- Form -----------------------
  afterSelect() {
    let selectionExtent = this.getSelectExtent(this.select);
      this.mapZoom.getView().fit(selectionExtent, this.mapZoom.getSize());
    this.selectZoom.getFeatures().clear();
    this.selectZoom.getFeatures().extend(this.select.getFeatures().getArray());

    this.selectedText = TextExtractor.extractText(
      this.selectedFeatures,
      (feature: ol.Feature) => feature.get('text'),
      (feature: ol.Feature) => feature.get('textIndex'),
    );

    this.setCurrentFieldValue(this.selectedText);
    this.nextField();
  }
  private initForm() {
    this.fields = [1, 2, 3, 4, 5, 6].map(field => ({
      name: 'f' + field,
      value: '',
    }));
    setTimeout(() => this.firstField(), 0);
  }
  private setCurrentFieldValue(value) {
    let field = this.fields[this.fieldIndex];
    if (field) {
      field.value = this.selectedText;
    }
  }
  private firstField() {
    this.fieldIndex = 0;
    this.focusField();
  }
  private nextField() {
    this.fieldIndex++;
    this.focusField();
  }
  private focusField() {
    let input = this.getCurrentInput();
    input && input.focus();
  }
  private getCurrentInput() {
    return this.form.nativeElement.getElementsByTagName('input')[this.fieldIndex];
  }
  private getSelectExtent(select: ol.interaction.Select) {
    return select.getFeatures().getArray()
      .map(feature => feature.getGeometry().getExtent())
      .reduce(
        (fullExtent, featureExtent) => ol.extent.extend(fullExtent, featureExtent),
        ol.extent.createEmpty()
      );
  }

}
