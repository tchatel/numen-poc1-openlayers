import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import "rxjs/add/operator/map";
import {Observable} from "rxjs";

interface JsonRectCoord {
  x: number;  // x
  y: number;  // y
  w: number;  // width
  h: number;  // height
}
interface JsonWord {
  e: number;        // ????
  c: JsonRectCoord; // coordonnées du rectangle
  t: string;        // texte
  s: number;        // ????
  i?: number;       // index, pas dans le fichier d'origine mais rajouté après lecture
}
interface JsonCapture {
  width: number;  // largeur totale
  height: number; // hauteur totale
  resX: number;   // résolution (300dpi)
  resY: number;   // résolution (300dpi)
  words: JsonWord[];
}

export interface Capture {
  width: number;  // largeur totale
  height: number; // hauteur totale
  features: ol.Collection<ol.Feature>;
}

@Injectable()
export class CaptureService {
  constructor(
    private http: Http
  ) { }

  urlPdf(directory: string) {
    return this.fullDirectory(directory) + 'scan.pdf';
  }
  urlImage(directory: string, type: string = 'png') {
    return this.fullDirectory(directory) + 'scan.' + type;
  }
  urlThumbnail(directory: string) {
    return this.fullDirectory(directory) + 'thumbnail.jpg';
  }
  urlJson(directory: string) {
    return this.fullDirectory(directory) + 'capture.json';
  }
  private fullDirectory(directory: string) {
    return `assets/data/${directory}/`;
  }

  loadFeatures(directory: string): Observable<Capture> {
    return this.loadJson(directory)
      .map(jsonCapture => {
        let collection = new ol.Collection<ol.Feature>();
        jsonCapture.words.forEach(jsonWord => {
          let featureData = {
            geometry: this.getBox(jsonWord, jsonCapture),
            labelPoint: new ol.geom.Point(this.getCenter(jsonWord, jsonCapture)),
            text: this.getText(jsonWord),
            textIndex: this.getTextIndex(jsonWord),
            json: jsonWord  // Temporary ?
          };
          collection.push(new ol.Feature(featureData));
        });
        console.log("collection length", collection.getLength());
        return {
          width: jsonCapture.width,
          height: jsonCapture.height,
          features: collection
        };
      });
  }
  private loadJson(directory: string): Observable<JsonCapture> {
    return this.http.get(this.urlJson(directory))
      .map(response => response.json())
      .map(json => {
        json.width = +json.width;
        json.height = +json.height;
        json.resX = +json.resX;
        json.resY = +json.resY;
        json.words = json.words.map((word: any, index: number) => ({
          i: index,
          c: {
            h: +word.c.h,
            w: +word.c.w,
            x: +word.c.x,
            y: +word.c.y,
          },
          t: word.t,
        }));
        return json;
      });
  }

  private getText(word: JsonWord): string {
    return word.t;
  }
  private getTextIndex(word: JsonWord): number {
    return word.i;
  }
  private getCenter(word: JsonWord, capture: JsonCapture): ol.Coordinate {
    return [
      word.c.x + Math.round(word.c.w / 2),
      capture.height - word.c.y + Math.round(word.c.h / 2)
    ];
  }
  private getBox(word: JsonWord, capture: JsonCapture): ol.geom.Geometry {
    let extent = ol.extent.boundingExtent([
      [
        word.c.x,
        capture.height - word.c.y
      ],
      [
        word.c.x + word.c.w,
        capture.height - (word.c.y + word.c.h)
      ],
    ]);
    let geometry = new ol.geom.Polygon(null);
    geometry.setCoordinates([[
      ol.extent.getBottomLeft(extent),
      ol.extent.getBottomRight(extent),
      ol.extent.getTopRight(extent),
      ol.extent.getTopLeft(extent),
      ol.extent.getBottomLeft(extent)
    ]]);
    return geometry;
  }

  // Test
  private getSampleFeature() {
    let w = {
      e:0,
      c: {
        x: 0,
        y: 0,
        w: 1000,
        h: 500
      },
      t: 'Test',
      s: 0
    };
    // return new ol.Feature({
    //   geometry: this.getBox(w),
    //   labelPoint: new ol.geom.Point(this.getCenter(w)),
    //   name: this.getText(w)
    // });
  }

}
