import { Component, OnInit } from '@angular/core';
import {CaptureService} from "../capture.service";

@Component({
  selector: 'app-postit',
  templateUrl: './postit.component.html',
  styleUrls: ['./postit.component.css']
})
export class PostitComponent implements OnInit {

  constructor(private capture: CaptureService) {
  }

  createTextStyle(feature: (ol.Feature | ol.render.Feature), resolution: number) {
    console.log(resolution);
    let fontSize = Math.round(30 / resolution);
    return new ol.style.Text({
      textAlign: 'left',          // ["center", "end", "left", "right", "start]
      textBaseline: 'baseline',   // ["alphabetic", "bottom", "hanging", "ideographic", "middle", "top"]
      font: `bold ${fontSize}px Verdana`,  // normal /  ['Arial', 'Courier New', 'Verdana', 'Quattrocento Sans']
      text: feature.get('text'),
      fill: new ol.style.Fill({color: 'blue'}),
      stroke: new ol.style.Stroke({color: 'white', width: 1}),
      offsetX: -90,   // 10
      offsetY: 0,   // 10
      rotation: 0
    });
  }

  createLayerStyle(feature: (ol.Feature | ol.render.Feature), resolution: number) {
    return new ol.style.Style({
      stroke: new ol.style.Stroke({color: 'red', width: 2}),
      fill: new ol.style.Fill({color: 'yellow'}),
      text: this.createTextStyle(feature, resolution),
    });
  }

  ngOnInit() {

    const url = this.capture.urlImage('04bi719');

    // Map views always need a projection.  Here we just want to map image
    // coordinates directly to map coordinates, so we create a projection that uses
    // the image extent in pixels.
    const extent: ol.Extent = [0, 0, 2476, 3510];
    let projection = new ol.proj.Projection({
      code: 'xkcd-image',
      units: 'pixels',
      extent: extent
    });

    let postitPos = {x: 1000, y: 1650, w: 400, h: 300};
    let postitExtent: ol.Extent = [postitPos.x, postitPos.y, postitPos.x + postitPos.w, postitPos.y + postitPos.h];
    let postitFeature = new ol.Feature({
      geometry: this.createBoxGeometry(postitExtent),
      text: this.stringDivider("Post-it : une annotation avec un texte sur plusieurs lignes", 20, '\n'),
      // text: "Post-it :\nune annotation avec un texte\nsur plusieurs lignes",
    });

    let iconFeature = new ol.Feature({
      geometry: new ol.geom.Point([800, 1500]),
    });
    iconFeature.setStyle(new ol.style.Style({
      image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
        anchor: [0.8, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: 'assets/postit1.png'
      }))
    }));

    let features = new ol.Collection<ol.Feature>();
    features.push(postitFeature);
    features.push(iconFeature);

    let map = new ol.Map({
      layers: [
        new ol.layer.Image({
          source: new ol.source.ImageStatic({
            attributions: '© <a href="http://xkcd.com/license.html">xkcd</a>',
            url: url,
            projection: projection,
            imageExtent: extent
          })
        }),
        new ol.layer.Vector({
          source: new ol.source.Vector({
            features: features
          }),
          style: (feature, resolution) => this.createLayerStyle(feature, resolution)
        })
      ],
      target: 'map',
      // view: new ol.View({
      //   projection: projection,
      //   center: ol.extent.getCenter(extent),
      //   zoom: 2,
      //   maxZoom: 8
      // }),
      view: new ol.View({
        projection: projection,
        center: ol.extent.getCenter(extent),
        resolution: 2,
        // zoom: 2,
        // maxZoom: 8
      }),
    });

  }

  // http://stackoverflow.com/questions/14484787/wrap-text-in-javascript
  private stringDivider(str, width, spaceReplacer) {
    if (str.length > width) {
      var p = width;
      while (p > 0 && (str[p] != ' ' && str[p] != '-')) {
        p--;
      }
      if (p > 0) {
        var left;
        if (str.substring(p, p + 1) == '-') {
          left = str.substring(0, p + 1);
        } else {
          left = str.substring(0, p);
        }
        var right = str.substring(p + 1);
        return left + spaceReplacer + this.stringDivider(right, width, spaceReplacer);
      }
    }
    return str;
  }

  private createBoxGeometry(extent: ol.Extent) {
    let geometry = new ol.geom.Polygon(null);
    geometry.setCoordinates([[
      ol.extent.getBottomLeft(extent),
      ol.extent.getBottomRight(extent),
      ol.extent.getTopRight(extent),
      ol.extent.getTopLeft(extent),
      ol.extent.getBottomLeft(extent)
    ]]);
    return geometry;
  }

}
