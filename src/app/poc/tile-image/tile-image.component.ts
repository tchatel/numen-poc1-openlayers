import { Component, OnInit } from '@angular/core';
import {CaptureService, Capture} from "../capture.service";
import {TextExtractor} from "../text-extractor.model";
import {OptionsService} from "../options.service";


interface MapData {
  directory: string;
  imageUrl: string;
  capture: Capture;
}

@Component({
  selector: 'app-tile-image',
  templateUrl: 'tile-image.component.html',
  styleUrls: ['tile-image.component.css']
})
export class TileImageComponent implements OnInit {

  map: ol.Map;
  imageLayer: ol.layer.Tile;
  vectorLayer: ol.layer.Vector;
  mapData: MapData;
  selectedFeatures = new ol.Collection<ol.Feature>();
  selectedText: string;
  newLine = true;

  constructor(
    private capture: CaptureService,
    private options: OptionsService,
  ) { }

  getSelectedText(newLine: boolean = true) {
    return newLine
      ? this.selectedText && this.selectedText.replace(/\n/g, '<br>')
      : this.selectedText;
  }

  toggleLayers() {
    if (this.map) {
      this.imageLayer.setVisible(this.options.showImageLayer);
      this.vectorLayer.setVisible(this.options.showVectorLayer);
    }
  }

  ngOnInit() {

    const directory = '04bi719';
    const imageUrl = this.capture.urlImage('04bi719');
    this.capture.loadFeatures(directory)
      .map((capture: Capture): MapData => ({directory, imageUrl, capture}))
      .subscribe((mapData: MapData) => {
        this.mapData = mapData;
        this.init(mapData);
      });
  }

  init(mapData: MapData) {
    // Map views always need a projection.  Here we just want to map image coordinates directly
    // to map coordinates, so we create a projection that uses the image extent in pixels.
    const imgExtent:ol.Extent = [0, 0, 10*256, 14*256];
    let projection = new ol.proj.Projection({
      code: 'xkcd-image',
      units: 'pixels',
      extent: imgExtent
    });

    let tileSource = new ol.source.TileImage({
      projection: projection,
      tileGrid: new ol.tilegrid.TileGrid({
        extent: imgExtent,
        tileSize: [256, 256],
        origin: [0, 0],
        resolutions: [16, 8, 4, 2, 1]
      }),
      tileUrlFunction: function(coordinate) {
        return `assets/data/tiles-cubic/${coordinate[0]}/${coordinate[1]}/${coordinate[2]}.png`;
      }
    });

    this.imageLayer = new ol.layer.Tile({
      source: tileSource,
    });
    this.vectorLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: mapData.capture.features,
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'red', width: 2})
      })
    });
    this.map = new ol.Map({
      controls: [
        // new ol.control.MousePosition(),
        new ol.control.Zoom(),
        new ol.control.ZoomSlider(),
      ],
      layers: [
        this.imageLayer,
        this.vectorLayer,
      ],
      target: 'map',
      view: new ol.View({
        projection: projection,
        extent: imgExtent,
        center: ol.extent.getCenter(imgExtent),
        resolution: 4,
        minResolution: 1,
        maxResolution: 16,
        resolutions: [16, 8, 4, 2, 1]
        // zoom: 2,
        // minZoom: 0,
        // maxZoom: 4
      })
    });
    this.toggleLayers();

    // SELECTION ----------------------------------------
    // a normal select interaction to handle click
    let select = new ol.interaction.Select();
    this.map.addInteraction(select);
    // a DragBox interaction used to select features by drawing boxes
    let dragBox = new ol.interaction.DragBox({
      condition: ol.events.condition.platformModifierKeyOnly
    });
    this.map.addInteraction(dragBox);
    dragBox.on('boxend', () => {
      // features that intersect the box are added to the collection of selected features
      let selectedExtent = dragBox.getGeometry().getExtent();
      this.vectorLayer.getSource().forEachFeatureIntersectingExtent(selectedExtent, (feature) => {
        this.selectedFeatures.push(feature);
      });
      this.selectedText = TextExtractor.extractText(
        this.selectedFeatures,
        (feature: ol.Feature) => feature.get('text'),
        (feature: ol.Feature) => feature.get('textIndex'),
      );
    });
    // clear selection when drawing a new box and when clicking on the map
    dragBox.on('boxstart', () => {
      this.selectedFeatures.clear();
    });
    this.map.on('click', () => {
      this.selectedFeatures.clear();
    });



    // =============== OLD TESTS ==============================
    if (2>1) return;

    // let imgWidth = 2476;
    // let imgHeight = 3510;
    // let imgCenter:ol.Coordinate = [imgWidth / 2, imgHeight / 2];

    // *** test, not working
    let osmSource = new ol.source.OSM({
      // attributions?: ol.AttributionLike;
      // cacheSize?: number;
      // crossOrigin?: (string);
      maxZoom: 4,
      // opaque?: boolean;
      // reprojectionErrorThreshold?: number;
      tileLoadFunction: (imageTile, src) => {
        console.log(imageTile, src);
        imageTile['getImage']().src = src;
      },
      url: 'assets/data/tiles/{z}/{x}/{-y}.png',
      // wrapX?: boolean;
    });
    // *** test, not working
    let tileSource1 = new ol.source.TileImage({
      projection: projection,
      tileUrlFunction: (tileCoords: ol.TileCoord, pixelRatio: number, projection: ol.proj.Projection) : string => {
        console.log('tileUrlFunction', 'zoom=', tileCoords[0], 'x=', tileCoords[1], 'y=', tileCoords[2]);
        return mapData.imageUrl + 'Zoomify_Image/1/' + tileCoords[0] + '-' + tileCoords[1] +
          '-' + (-tileCoords[2] - 1) + '.jpg';
      },
      tileGrid: new ol.tilegrid.TileGrid({
        extent: imgExtent,
        minZoom: 0,
        origin: [0, 0],
        tileSize: [2476, 3510],
        resolutions: [1, 2, 4, 8, 16].reverse()
      }),
      // tileGrid: ol.tilegrid.createXYZ({
      //   extent: extent,
      //   minZoom: -64,
      //   tileSize: [2476, 3510],
      // })
      // tileGrid: new ol.tilegrid.Zoomify({
      //   resolutions: [1, 2, 4, 8, 16, 32, 64].reverse()
      // })
    });
    // *** test, not working
    let tileSource2 = new ol.source.TileImage({
      // attributions?: ol.AttributionLike;
      // cacheSize?: number;
      // crossOrigin?: (string);
      // logo?: (string | olx.LogoOptions);
      // opaque?: boolean;
      projection: projection,
      // reprojectionErrorThreshold?: number;
      // state?: ol.source.State;
      // tileClass?: ((n: ol.ImageTile, coords: ol.TileCoord, state: ol.Tile.State, s1: string, s2: string, type: ol.TileLoadFunctionType) => any);
      tileGrid: new ol.tilegrid.TileGrid({
        extent: imgExtent,
        minZoom: 0,
        // origin: [0, 0],
        // tileSize: [2476, 3510],
        resolutions: [0, 1, 2, 3, 4].reverse()
      }),
      // tileLoadFunction?: ol.TileLoadFunctionType;
      // tilePixelRatio?: number;
      // tileUrlFunction?: ol.TileUrlFunctionType;
      url: 'assets/data/tiles/{z}/{x}/{-y}.png',
      // urls?: string[];
      // wrapX?: boolean;
    });


    /* exemple : https://jsfiddle.net/4snbzL1r/2/  */
    // {
    //   let tms_resolutions_3857_tynset = [5.8135986328125, 2.90679931640625, 1.45339965820313, 0.726699829101563, 0.363349914550781, 0.181674957275391, 0.0908374786376953, 0.0454187393188477];
    //   let extent_3857_tynset: ol.Extent = [1199098.1904393, 8923848.54918488, 1199956.06726346, 8924597.49205864];
    //   let projection_3857_tynset = new ol.proj.Projection({
    //     code: 'xkcd-image',
    //     units: 'pixels',
    //     extent: extent_3857_tynset
    //   });
    //
    //   let map = new ol.Map({
    //     layers: [
    //       // new ol.layer.Tile({
    //       //   source: new ol.source.OSM()
    //       // }),
    //       new ol.layer.Tile({
    //         preload: 1,
    //         opacity: 0.8,
    //         source: new ol.source.TileImage({
    //           crossOrigin: null,
    //           projection: projection_3857_tynset,
    //           // extent: extent_3857_tynset,
    //           tileGrid: new ol.tilegrid.TileGrid({
    //             extent: extent_3857_tynset,
    //             origin: [extent_3857_tynset[0], extent_3857_tynset[1]],
    //             resolutions: tms_resolutions_3857_tynset
    //           }),
    //           tileUrlFunction: function (coordinate) {
    //             console.log('tileUrlFunction', 'zoom=', coordinate[0], 'x=', coordinate[1], 'y=', coordinate[2]);
    //             if (coordinate === null) return undefined;
    //
    //             // TMS Style URL
    //             var z = coordinate[0];
    //             var x = coordinate[1];
    //             var y = coordinate[2];
    //             var url = 'https://quadridcmmaps.blob.core.windows.net/tynset/' + z + '/' + x + '/' + y + '.png';
    //             return url;
    //           }
    //         })
    //       })
    //     ],
    //     target: 'map',
    //     view: new ol.View({
    //       center: [1200512.8002755763, 8925231.712804087],
    //       //center: [8923848.54918488, 1199098.1904393],
    //       zoom: 14
    //     })
    //   });
    // }
  }

}
