import { TestBed, inject } from '@angular/core/testing';

import { CaptureService } from './capture.service';

describe('CaptureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CaptureService]
    });
  });

  it('should ...', inject([CaptureService], (service: CaptureService) => {
    expect(service).toBeTruthy();
  }));
});
