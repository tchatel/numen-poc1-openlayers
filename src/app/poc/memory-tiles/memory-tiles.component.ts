import {Component, OnInit} from '@angular/core';
import {CaptureService, Capture} from "../capture.service";
import {TextExtractor} from "../text-extractor.model";
import {OptionsService} from "../options.service";

interface MapData {
  directory: string;
  imageUrl: string;
  capture: Capture;
}

interface Size {
  width: number;
  height: number;
}
class ImageCanvas {
  constructor(
    public canvas: HTMLCanvasElement | HTMLImageElement,
    public canvasSize: Size,
    public imageSize: Size,
  ) { }
  public getContext() {
    return this.canvas instanceof HTMLCanvasElement ? this.canvas.getContext('2d') : null;
  }
}

@Component({
  selector: 'app-memory-tiles',
  templateUrl: './memory-tiles.component.html',
  styleUrls: ['./memory-tiles.component.css']
})
export class MemoryTilesComponent implements OnInit {

  map: ol.Map;
  imageLayer: ol.layer.Tile;
  vectorLayer: ol.layer.Vector;
  mapData: MapData;
  selectedFeatures = new ol.Collection<ol.Feature>();
  selectedText: string;
  newLine = true;

  constructor(private capture: CaptureService,
              private options: OptionsService,) {
  }

  getSelectedText(newLine: boolean = true) {
    return newLine
      ? this.selectedText && this.selectedText.replace(/\n/g, '<br>')
      : this.selectedText;
  }

  toggleLayers() {
    if (this.map) {
      this.imageLayer && this.imageLayer.setVisible(this.options.showImageLayer);
      this.vectorLayer && this.vectorLayer.setVisible(this.options.showVectorLayer);
    }
  }

  ngOnInit() {
    const directory = '04bi719';
    const imageUrl = this.capture.urlImage('04bi719');
    this.capture.loadFeatures(directory)
      .map((capture: Capture): MapData => ({directory, imageUrl, capture}))
      .subscribe((mapData: MapData) => {
        this.mapData = mapData;
        this.init(mapData);
      });
  }


  buildCanvas(size: Size): HTMLCanvasElement {
    const canvas = document.createElement('canvas');
    canvas.width = size.width;
    canvas.height = size.height;
    return canvas;
  }
  buildExtentedOriginalImageCanvas(image: HTMLImageElement, fullSize: Size): ImageCanvas {
    let imageSize = {width: image.naturalWidth, height: image.naturalHeight};
    let src = new ImageCanvas(image, imageSize, imageSize);
    let dst = new ImageCanvas(this.buildCanvas(fullSize), fullSize, src.imageSize);
    this.drawImage(src, dst);
    return dst;
  }
  drawImage(src: ImageCanvas, dst: ImageCanvas): void {
    let ctx = dst.getContext();
    // Image must be at the bottom left of the canvas
    ctx.drawImage(
      src.canvas,
      0, src.canvas.height - src.imageSize.height, src.imageSize.width, src.imageSize.height,  // source
      0, dst.canvas.height - dst.imageSize.height, dst.imageSize.width, dst.imageSize.height,  // destination
    );
  }
  nextZoom(src: ImageCanvas, tileSize: Size): ImageCanvas {
    let dstImageSize = {
      width: Math.floor(src.imageSize.width / 2),
      height: Math.floor(src.imageSize.height / 2)
    };
    let dstGrid = this.getGrid(dstImageSize, tileSize);
    let dst = new ImageCanvas(
      this.buildCanvas(dstGrid.fullSize),
      dstGrid.fullSize,
      dstImageSize,
    );
    this.drawImage(src, dst);
    return dst;
  }
  getGrid(imageSize: Size, tileSize: Size) {
    const x = Math.ceil(imageSize.width / tileSize.width);
    const y = Math.ceil(imageSize.height / tileSize.height);
    const fullSize = {
      width: x * tileSize.width,
      height: y * tileSize.height,
    };
    return {x, y, fullSize};
  }

  // imageCanvas(image, width: number, height: number) {
  //   const newCanvas = document.createElement('canvas');
  //   newCanvas.width = width;
  //   newCanvas.height = height;
  //   let ctx = newCanvas.getContext('2d');
  //   ctx.drawImage(image, 0, 0, width, height);
  //   return newCanvas;
  // }
  // resizeDownCanvas(previousCanvas: HTMLCanvasElement) {
  //   return this.imageCanvas(
  //     previousCanvas,
  //     Math.round(previousCanvas.width / 2),
  //     Math.round(previousCanvas.height / 2)
  //   );
  // }
  // extendCanvas(previousCanvas: HTMLCanvasElement, fullWidth: number, fullHeight: number) {
  //   const newCanvas = document.createElement('canvas');
  //   newCanvas.width = fullWidth;
  //   newCanvas.height = fullHeight;
  //   const marginX = Math.round((fullWidth - previousCanvas.width) / 2);
  //   const marginY= Math.round((fullHeight - previousCanvas.height) / 2);
  //   let ctx = newCanvas.getContext('2d');
  //   // ctx.drawImage(previousCanvas, marginX, marginY, previousCanvas.width, previousCanvas.height);
  //   ctx.drawImage(
  //     previousCanvas,
  //     0, fullHeight - previousCanvas.height,
  //     previousCanvas.width, previousCanvas.height
  //   );
  //   return newCanvas;
  // }
  generateTiles(image: HTMLImageElement, tileSize: Size, levels: number = 5):
  Promise<{zoom: number; tiles: Array<Array<string>>;}[]> {
    const levelPromiseArray = [];
    const imageSize = {width: image.naturalWidth, height: image.naturalHeight};
    let fullSizeGrid = this.getGrid(imageSize, tileSize);
    let imageCanvas = this.buildExtentedOriginalImageCanvas(image, fullSizeGrid.fullSize);
    for (let zoom = levels - 1; zoom >= 0; zoom--) {
      let resolution = Math.pow(2, levels - 1 - zoom); // zoom max => resolution = 1
      levelPromiseArray[zoom] = this.cutTiles(imageCanvas, tileSize)
        .then(tiles => {
          console.log({zoom, tiles});
          return {zoom, tiles};
        });
      imageCanvas = this.nextZoom(imageCanvas, tileSize);
    }
    return Promise.all(levelPromiseArray);
  }

  cutTiles(src: ImageCanvas, tileSize: Size): Promise<Array<Array<string>>> {
    const grid = this.getGrid(src.imageSize, tileSize);
    console.log('grid', grid);
    let arrayX = this.buildNumbersArray(grid.x);
    let arrayY = this.buildNumbersArray(grid.y);
    return Promise.all(arrayY.map(
      x => Promise.all(arrayY.map(
        y => this.getTileUrl(src, tileSize, x, y)))
      )
    );
  }

  // X from left, Y from bottom
  getTileUrl(src: ImageCanvas, tileSize: Size, tileX: number, tileY: number): Promise<string> {
    const newCanvas = this.buildCanvas(tileSize);
    let ctx = newCanvas.getContext('2d');
    ctx.drawImage(
      src.canvas,
      tileX * tileSize.width, tileY * tileSize.height, tileSize.width, tileSize.height, // source
      0, 0, tileSize.width, tileSize.height // destination
    );
    return new Promise((resolve) => {
      newCanvas.toBlob((blob) => {
        resolve(window.URL.createObjectURL(blob));
      });
    });
  }

  buildNumbersArray(size: number): number[] {
    let array: number[] = [];
    for (let i = 0; i < size; i++) {
      array.push(i);
    }
    return array;
  }

  // TODO : agrandir les images générées à la taille maximale ?


  init(mapData: MapData) {
    // Map views always need a projection.  Here we just want to map image coordinates directly
    // to map coordinates, so we create a projection that uses the image extent in pixels.
    const imgUrl = 'assets/data/04bi719/scan.png';
    const imgExtent: ol.Extent = [0, 0, 2476, 3510];

    let projection = new ol.proj.Projection({
      code: 'xkcd-image',
      units: 'pixels',
      extent: imgExtent
    });

    this.vectorLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: mapData.capture.features,
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'red', width: 2})
      })
    });
    this.map = new ol.Map({
      controls: [
        // new ol.control.MousePosition(),
        new ol.control.Zoom(),
        new ol.control.ZoomSlider(),
      ],
      layers: [
        // this.imageLayer,
        this.vectorLayer,
      ],
      target: 'map',
      view: new ol.View({
        projection: projection,
        extent: projection.getExtent(),
        center: ol.extent.getCenter(imgExtent),
        resolution: 4,
        minResolution: 1,
        maxResolution: 16,
        resolutions: [16, 8, 4, 2, 1]
        // zoom: 2,
        // minZoom: 0,
        // maxZoom: 4
      })
    });
    this.toggleLayers();

    const image = new Image();
    image.src = imgUrl;
    image.onload = () => {
      this.generateTiles(image, {width: 256, height: 256}).then(array => {
        let tileSource = new ol.source.TileImage({
          projection: projection,
          tileGrid: new ol.tilegrid.TileGrid({
            extent: projection.getExtent(),
            tileSize: [256, 256],
            origin: [0, 0],
            resolutions: [16, 8, 4, 2, 1]
          }),
          tileUrlFunction: function (coordinate) { // z, x, y
            let z = coordinate[0];
            let tiles = array[z].tiles;
            let x = coordinate[1];
            let y = coordinate[2];
            y = tiles[x].length - 1 - y; // must revert y !
            return tiles[x][y];
            // return `assets/data/tiles-cubic/${coordinate[0]}/${coordinate[1]}/${coordinate[2]}.png`;
          }
        });
        this.imageLayer = new ol.layer.Tile({
          source: tileSource,
        });
        this.map.getLayers().insertAt(0, this.imageLayer);
        this.toggleLayers();
      });
    };


    // // SELECTION ----------------------------------------
    // // a normal select interaction to handle click
    // let select = new ol.interaction.Select();
    // this.map.addInteraction(select);
    // // a DragBox interaction used to select features by drawing boxes
    // let dragBox = new ol.interaction.DragBox({
    //   condition: ol.events.condition.platformModifierKeyOnly
    // });
    // this.map.addInteraction(dragBox);
    // dragBox.on('boxend', () => {
    //   // features that intersect the box are added to the collection of selected features
    //   let selectedExtent = dragBox.getGeometry().getExtent();
    //   this.vectorLayer.getSource().forEachFeatureIntersectingExtent(selectedExtent, (feature) => {
    //     this.selectedFeatures.push(feature);
    //   });
    //   this.selectedText = TextExtractor.extractText(
    //     this.selectedFeatures,
    //     (feature: ol.Feature) => feature.get('text'),
    //     (feature: ol.Feature) => feature.get('textIndex'),
    //   );
    // });
    // // clear selection when drawing a new box and when clicking on the map
    // dragBox.on('boxstart', () => {
    //   this.selectedFeatures.clear();
    // });
    // this.map.on('click', () => {
    //   this.selectedFeatures.clear();
    // });

  }

}

