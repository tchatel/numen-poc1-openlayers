import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemoryTilesComponent } from './memory-tiles.component';

describe('MemoryTilesComponent', () => {
  let component: MemoryTilesComponent;
  let fixture: ComponentFixture<MemoryTilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemoryTilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoryTilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
