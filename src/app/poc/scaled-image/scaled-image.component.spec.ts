import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScaledImageComponent } from './scaled-image.component';

describe('ScaledImageComponent', () => {
  let component: ScaledImageComponent;
  let fixture: ComponentFixture<ScaledImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScaledImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScaledImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
