import {Component, OnInit} from '@angular/core';
import {CaptureService} from "../capture.service";


// Pas bon, la méthode handleImageChange n'est pas appelée lors du zoom, seulement au premier affichage de l'image
class ImageStaticResize extends ol.source.ImageStatic {
  handleImageChange(evt) {
    console.log('***** handleImageChange *****');
    if (this['image_'].getState() == ol['ImageState'].LOADED) {
      let imageExtent = this['image_'].getExtent();
      let image = this['image_'].getImage();
      let imageWidth, imageHeight;
      if (this['imageSize_']) {
        imageWidth = this['imageSize_'][0];
        imageHeight = this['imageSize_'][1];
      } else {
        imageWidth = image.width;
        imageHeight = image.height;
      }
      let resolution = ol.extent.getHeight(imageExtent) / imageHeight;
      let targetWidth = Math.ceil(ol.extent.getWidth(imageExtent) / resolution);
      if (targetWidth != imageWidth) {
        let context = ol['dom'].createCanvasContext2D(targetWidth, imageHeight);
        let canvas = context.canvas;
        context.drawImage(image, 0, 0, imageWidth, imageHeight,
          0, 0, canvas.width, canvas.height);
        this['image_'].setImage(canvas);
      }
      // THIERRY :
      // if (targetWidth != imageWidth /* THIERRY */ && (imageWidth / targetWidth) >= 6) {
      //   console.log(888888)
      //   let context0 = ol['dom'].createCanvasContext2D(Math.round(imageWidth / 8), Math.round(imageHeight/8));
      //   let canvas0 = context0.canvas;
      //   context0.drawImage(image, 0, 0, imageWidth, imageHeight,
      //     0, 0, canvas0.width, canvas0.height);
      //
      //   let context1 = ol['dom'].createCanvasContext2D(targetWidth, imageHeight);
      //   let canvas1 = context1.canvas;
      //   context1.drawImage(canvas0, 0, 0, canvas0.width, canvas0.height,
      //     0, 0, canvas1.width, canvas1.height);
      //
      //   this['image_'].setImage(canvas1);
      // }
    }
    ol.source.Image.prototype['handleImageChange'].call(this, evt);
  }
}


@Component({
  selector: 'app-scaled-image',
  templateUrl: './scaled-image.component.html',
  styleUrls: ['./scaled-image.component.css']
})
export class ScaledImageComponent implements OnInit {

  map: ol.Map;
  layers: ol.layer.Image[];

  constructor(private capture: CaptureService) {
  }

  layersInfo() {
    return this.map.getLayers().getArray().map(layer => ` [${layer.getMinResolution()}, ${layer.getMaxResolution()}[ `);
  }

  layersWithOneImage(projection: ol.proj.Projection, extent: ol.Extent) {
    return [
      new ol.layer.Image({
        source: new ol.source.ImageStatic({
          url: 'assets/data/04bi719/scan.png',
          projection: projection,
          imageExtent: extent
        }),
      }),
    ];
  }

  resizeForResolution(image: HTMLImageElement|HTMLCanvasElement, resolution: number): Promise<{canvas: HTMLCanvasElement, url: string}> {
      const canvas = document.createElement('canvas');
      canvas.width = Math.round(image.width / resolution);
      canvas.height = Math.round(image.height / resolution);
      let ctx = canvas.getContext('2d');
      ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, canvas.width, canvas.height);
      return new Promise((resolve) => {
        canvas.toBlob((blob) => {
          let url = window.URL.createObjectURL(blob);
          resolve({canvas, url});
        });
      });
  }
  layersWithTwoImages(projection: ol.proj.Projection, extent: ol.Extent, image: HTMLImageElement): Promise<ol.layer.Image[]> {
    // Utiliser aussi les résolutions 2 et 4 ?
    let res2Promise = this.resizeForResolution(image, 2);
    let res4Promise = res2Promise.then(({canvas}) => this.resizeForResolution(canvas, 2));
    let res8Promise = res4Promise.then(({canvas}) => this.resizeForResolution(canvas, 2));
    return new Promise((resolve) => {
      res8Promise.then(({url}) => {
        resolve([
          new ol.layer.Image({
            source: new ol.source.ImageStatic({
              url: image.src,
              projection: projection,
              imageExtent: extent
            }),
            maxResolution: 6,  // exclusive
          }),
          new ol.layer.Image({
            source: new ol.source.ImageStatic({
              url: url,
              projection: projection,
              imageExtent: extent
            }),
            minResolution: 6,  // inclusive
          }),
        ]);
      });
    });
  }

  layersWithSeveralResizedImages(projection: ol.proj.Projection, extent: ol.Extent) {
    return [
      new ol.layer.Image({
        source: new ol.source.ImageStatic({
          url: 'assets/data/04bi719/scan-res-1.png',
          projection: projection,
          imageExtent: extent
        }),
        maxResolution: 1.8,  // exclusive
      }),
      new ol.layer.Image({
        source: new ol.source.ImageStatic({
          url: 'assets/data/04bi719/scan-res-2.png',
          projection: projection,
          imageExtent: extent
        }),
        minResolution: 1.8,  // exclusive
        maxResolution: 3,  // exclusive
      }),
      new ol.layer.Image({
        source: new ol.source.ImageStatic({
          url: 'assets/data/04bi719/scan-res-4.png',
          projection: projection,
          imageExtent: extent
        }),
        minResolution: 3,  // exclusive
        maxResolution: 7,  // exclusive
      }),
      new ol.layer.Image({
        source: new ol.source.ImageStatic({
          url: 'assets/data/04bi719/scan-res-8.png',
          projection: projection,
          imageExtent: extent
        }),
        minResolution: 7,  // exclusive
        maxResolution: 15,  // exclusive
      }),
      new ol.layer.Image({
        source: new ol.source.ImageStatic({
          url: 'assets/data/04bi719/scan-res-16.png',
          projection: projection,
          imageExtent: extent
        }),
        minResolution: 15,  // exclusive
      }),
    ];
  }


  ngOnInit() {

    const url = this.capture.urlImage('04bi719');

    // Map views always need a projection.  Here we just want to map image
    // coordinates directly to map coordinates, so we create a projection that uses
    // the image extent in pixels.
    const extent: ol.Extent = [0, 0, 2476, 3510];
    let projection = new ol.proj.Projection({
      code: 'xkcd-image',
      units: 'pixels',
      extent: extent
    });

    this.map = new ol.Map({
      // layers: this.layers,
      target: 'map',
      view: new ol.View({
        projection: projection,
        center: ol.extent.getCenter(extent),
        resolution: 2,
        maxResolution: 16
      })
    });
    // this.map.on('precompose', function(evt) {
    //   evt.context.imageSmoothingEnabled = false;
    //   evt.context.webkitImageSmoothingEnabled = false;
    //   evt.context.mozImageSmoothingEnabled = false;
    //   evt.context.msImageSmoothingEnabled = false;
    // });

    const image = new Image();
    image.src = 'assets/data/04bi719/scan.png';
    image.onload = () => {
      // 1 taille
      // this.layers = this.layersWithOneImage(projection, extent);
      // this.layers.forEach(layer => this.map.addLayer(layer));

      // + 4 tailles générées avant
      // this.layers = this.layersWithSeveralResizedImages(projection, extent);
      // this.layers.forEach(layer => this.map.addLayer(layer));

      // + 1 taille générée à la volée
      this.layersWithTwoImages(projection, extent, image)
        .then(layers => {
          this.layers = layers;
          this.layers.forEach(layer => this.map.addLayer(layer));
        });

    };



    // this.map.getView().on('change:resolution', () => {
    //   let res = this.map.getView().getResolution();
    //   this.layers.forEach(layer => layer.setVisible(false));
    //   if (res > 15) {
    //     this.layers[4].setVisible(true);
    //   } else if (res > 7) {
    //     this.layers[3].setVisible(true);
    //   } else if (res > 3) {
    //     this.layers[2].setVisible(true);
    //   } else if (res > 1.5) {
    //     this.layers[1].setVisible(true);
    //   } else {
    //     this.layers[0].setVisible(true);
    //   }
    // });
  }

}
