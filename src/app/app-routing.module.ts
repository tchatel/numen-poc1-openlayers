import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {StaticImageComponent} from "./poc/static-image/static-image.component";
import {MapComponent} from "./poc/map/map.component";
import {MapVectorComponent} from "./poc/map-vector/map-vector.component";
import {ImageVectorComponent} from "./poc/image-vector/image-vector.component";
import {TileImageComponent} from "./poc/tile-image/tile-image.component";
import {MemoryTilesComponent} from "./poc/memory-tiles/memory-tiles.component";
import {CanvasTestComponent} from "./poc/canvas-test/canvas-test.component";
import {PostitComponent} from "./poc/postit/postit.component";
import {PdfComponent} from "./poc/pdf/pdf.component";
import {IndexComponent} from './poc/index/index.component';
import {ScaledImageComponent} from './poc/scaled-image/scaled-image.component';

const routes: Routes = [
  {path: 'index', component: IndexComponent},
  {path: 'canvas-test', component: CanvasTestComponent},
  {path: 'map', component: MapComponent},
  {path: 'map-vector', component: MapVectorComponent},
  {path: 'postit', component: PostitComponent},
  {path: 'static-image', component: StaticImageComponent},
  {path: 'scaled-image', component: ScaledImageComponent},
  {path: 'tile-image', component: TileImageComponent},
  {path: 'memory-tiles', component: MemoryTilesComponent},
  {path: 'image-vector/:directory/:type', component: ImageVectorComponent},
  {path: 'pdf', component: PdfComponent},
  {path: '', redirectTo: 'image-vector/04bi719/png', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
