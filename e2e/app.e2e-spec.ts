import { PocOlPage } from './app.po';

describe('poc-ol App', () => {
  let page: PocOlPage;

  beforeEach(() => {
    page = new PocOlPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
